export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
        host: string;
        port: number;
        password: string;
}

export interface Model {

    id: number;
    developer: string,
    company: string,
    individualPerformance: number,
    groupPerformance: number,
    attitudeBehavior: number,
    workgroup: number,
    adaptability: number,
    channel: string,
    createdAt: string;
    updatedAt: string;

}

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number,
}

export namespace Create{

    export interface Request{

        developer: string,
        company: string,
        individualPerformance?: number,
        groupPerformance?: number,
        attitudeBehavior?: number,
        workgroup?: number,
        adaptability?: number,
        channel: string

    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace Delete{

    export interface Request {

        developers?: string[],
        companies?: string[],
        channels?: string[],

    }

    export interface Response {
        statusCode: statusCode;
        data?: number;
        message?: string;
    }

}

export namespace Update{

    export interface Request {

        id: number;
        individualPerformance?: number,
        groupPerformance?: number,
        attitudeBehavior?: number,
        workgroup?: number,
        adaptability?: number,

    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace View{

    export interface Request {

        offset?: number;
        limit?: number;

        developer?: string,
        company?: string,
        channel?: string;

    }

    export interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }

}

export namespace FindOne{

    export interface Request {

        developer?: string,
        company?: string,

    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}
