# Api yfzmrwcderuulvd (Control de desarrolladores)

[![Travis][build-badge]][build]
[![npm package][npm-badge]][npm]
[![Coveralls][coveralls-badge]][coveralls]

The API contains basic functions to interact with the registry of developers. The functions are performed in typescript
but you can use javascript.

[build-badge]: https://img.shields.io/travis/user/repo/master.png?style=flat-square

[build]: https://travis-ci.org/user/repo

[npm-badge]: https://img.shields.io/npm/v/npm-package.png?style=flat-square

[npm]: https://www.npmjs.org/package/npm-package

[coveralls-badge]: https://img.shields.io/coveralls/user/repo/master.png?style=flat-square

[coveralls]: https://coveralls.io/github/user/repo


This is sponsored, supported, and affiliated with SARyS Inc.

visit our [Documetation for developers](https://developers.jwit.ar)

## Attention is only compatible with version 1

## Installation

```
npm install --save @api/yfzmrwcderuulvd
```

```
yarn add --save @api/yfzmrwcderuulvd
```

```
pnpm install @api/yfzmrwcderuulvd
```

## API Example

The Metods available and Example
-------
more example in folder tests

```js

import * as api from '@api/yfzmrwcderuulvd';

const redis = {
    local: {
        host: "192.168.88.246",
        port: 6379,
    },
    remote: {
        host: "sandbox.jwit.ar",
        port: 42518,
        password: "27bt0juzty775suc32ng42yzc"
    }
}

async function main() {
    try {

        const {statusCode, data, message} = await api.View({}, redis.local);

        console.log({statusCode, data, message})

    } catch (error) {
        console.log(error)
    }
}

main()
```

The Metods available
-------

With the available methods, you will be able to perform various tasks such as:

- `Create`
- `Delete`
- `Update`
- `FindOne`
- `View`

Create
-------

To create a registry, you must enter [developer, company, channel].

| params                | required |     type      |
|-----------------------|:--------:|:-------------:|
| developer             | &check;  |    string     |
| company               | &check;  |    string     |
| channel               | &check;  | UUID(string)  |
| adaptability          | &cross;  | number(0-100) |
| attitudeBehavior      | &cross;  | number(0-100) |
| groupPerformance      | &cross;  | number(0-100) |
| individualPerformance | &cross;  | number(0-100) |
| workgroup             | &cross;  | number(0-100) |

```js
import * as api from '@api/yfzmrwcderuulvd';

async function main() {
    try {

        const {statusCode, data, message} = await api.Create(
            {
                developer: "Anthony",
                company: "jwit",
                channel: "4c1ccc12-bdf3-11ec-9d64-0242ac120002",
                adaptability: 100,
                attitudeBehavior: 100,
                groupPerformance: 100,
                individualPerformance: 100,
                workgroup: 100
            },
            redis.local);

        console.log({statusCode, data, message})

    } catch (error) {
        console.log(error)
    }
}


main()
```

Update
-------

To update a registry it is necessary to send its id, and the other parameters, if they are not sent, it will not update
any
record.

| params                | required |     type      |
|-----------------------|:--------:|:-------------:|
| id                    | &check;  |    integer    |
| adaptability          | &cross;  | number(0-100) |
| attitudeBehavior      | &cross;  | number(0-100) |
| groupPerformance      | &cross;  | number(0-100) |
| individualPerformance | &cross;  | number(0-100) |
| workgroup             | &cross;  | number(0-100) |

```js
import * as api from '@api/yfzmrwcderuulvd';

async function main() {
    try {

        const {statusCode, data, message} = await api.Update(
            {
                id: 3,
                adaptability: 100,
                attitudeBehavior: 50,
                groupPerformance: 50,
                individualPerformance: 50,
                workgroup: 50
            },
            redis.local);

        console.log({statusCode, data, message})

    } catch (error) {

        console.log(error)

    }
}


main()
```

Delete
-------
To delete a record you must enter at least [developers, companies, channels].

| params     | required |        type         |
|------------|:--------:|:-------------------:|
| companies  | &cross;  |    array(string)    |
| developers | &cross;  |    array(string)    |
| channels   | &cross;  | array(string(UUID)) |

```js
import * as api from '@api/yfzmrwcderuulvd';

async function main() {

    try {

        const {statusCode, data, message} = await api.Delete(
            {

                companies: ["jwit"],
                developers: ["Developer Name"],
                channels: ["4c1ccc12-bdf3-11ec-9d64-0242ac120002"]

            }, redis.local);

        console.log({statusCode, data, message})

    } catch (error) {

        console.log(error)

    }
}


main()
```

FindOne
-------

You can search for a record by [developer, company].

| params    | required |  type   |
|-----------|:--------:|:-------:|
| developer | &cross;  | string  |
| company   | &cross;  | string  |

```js
import * as api from '@api/yfzmrwcderuulvd';

async function main() {
    try {

        const {statusCode, data, message} = await api.FindOne(
            {
                developer: 'Developer Name',
                company: 'jwit'
            }, redis.local);

        console.log({statusCode, data, message})

    } catch (error) {

        console.log(error)

    }
}


main()
```

View (findAndCountAll)
-------

This method no require parameter to use, but its have default parameters offset = 0, limit = 12 (Paginate).

| params    | required |     type     |
|-----------|:--------:|:------------:|
| offset    | &cross;  |   integer    |
| limit     | &cross;  |   integer    |
| channel   | &cross;  | string(UUID) |
| company   | &cross;  |    string    |
| developer | &cross;  |    string    |

```js
import * as api from '@api/yfzmrwcderuulvd';

async function main() {

    try {

        const {statusCode, data, message} = await api.View({
            channel: "4c1ccc12-bdf3-11ec-9d64-0242ac120002",
            limit: 1,
            offset: 0,
            developer: "Anthony",
            company: "jwit"
        }, redis.local);

        console.log({statusCode, data, message})

    } catch (error) {

        console.log(error)

    }
}


main()
```

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://developers.jwit.ar)
for commit guidelines.

## 1.0.0 (2022-04-16)

### Features

* **Initials project**

Copyright (c) 2022 SARyS Inc.








