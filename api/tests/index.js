const api = require('../dist');

const redis = {
    local: {
        host: "192.168.60.105",
        port: 6379,
        // password: undefined
    }
}

const environment = redis.local;

async function create() {

    try {

        const result = await api.Create({
            developer: "Anthony",
            company: "jwit",
            channel: "4c1ccc12-bdf3-11ec-9d64-0242ac120002",
            adaptability: 99,
            attitudeBehavior: 100,
            groupPerformance: 100,
            individualPerformance: 100,
            workgroup: 100
        }, environment);

        console.log(result);

    } catch (error) {
        console.error(error)
    }

}

async function del() {

    try {

        const result = await api.Delete({
            companies: ["jwit"],
            developers: ["Anthony"],
            channels: ["4c1ccc12-bdf3-11ec-9d64-0242ac120002"]
        }, environment);

        console.log(result);

    } catch (error) {
        console.error(error)
    }

}

async function update({id, adaptability, attitudeBehavior, groupPerformance, individualPerformance, workgroup}) {

    try {

        const result = await api.Update({
            id,
            adaptability,
            attitudeBehavior,
            groupPerformance,
            individualPerformance,
            workgroup
        }, environment);

        console.log(result);

    } catch (error) {
        console.error(error)
    }

}

async function findOne({developer, company}) {

    try {

        const result = await api.FindOne({developer, company}, environment);

        console.log(result);

    } catch (error) {
        console.error(error)
    }

}

async function view({channel, limit, offset, company, developer}) {

    try {

        const result = await api.View({
            channel,
            limit,
            offset,
            company,
            developer
        }, environment);

        console.log(result.data.data);

    } catch (error) {
        console.error(error)
    }

}

async function main() {

    try {

        await create();

        // await del();

        await view({});

        // await update({
        //     id: 6,
        //     adaptability: 100,
        //     attitudeBehavior: 50,
        //     groupPerformance: 50,
        //     individualPerformance: 50,
        //     workgroup: 50
        // });

        // await findOne({developer: 'Anthony', company: 'jwit'});
        //
        // await view({developer: "Anthony", company: "jwit", channel: "4c1ccc12-bdf3-11ec-9d64-0242ac120002"});

    } catch (e) {
        console.log(e);
    }

}

main();