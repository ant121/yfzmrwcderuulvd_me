import { InternalError, redisClient} from "../settings";
import { Controller as T} from "../types";

export async function Publish(props: T.Publish.Request): Promise<T.Publish.Response> {

    try{

        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return {statusCode: 'success', data: props}

    }catch (error) {

        console.error('error', { step: 'controller Publish', error});

        return { statusCode: 'error', message: InternalError};

    }
}