import * as T  from '../types';
import * as Models from "../models";
import {InternalError} from "../settings";
import * as Validation from "../../../validate/dist";

export async function create(params: T.Service.Create.Request): Promise<T.Service.Create.Response> {

    try{

        await Validation.create(params);

        const {statusCode, data, message} = await Models.create(params);

        return {statusCode, data, message};

    }catch (error) {

        console.error( { step: "Service create", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function del(params: T.Service.Delete.Request): Promise<T.Service.Delete.Response> {

    try{

        await Validation.del(params);

        let where: T.Models.Where = {}

        var optionals: T.Models.Attributes[] = ['developer', 'channel'];

        for( let x of optionals.map( v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0,-1)] = params[x];

        if (params.companies !== undefined) where.company = params.companies;

        const findOne = await Models.findOne({ where });

        if ( findOne.statusCode !== 'success' ){

            switch ( findOne.statusCode ) {

                case 'notFound' : return { statusCode: 'validationError', message: 'No existe el desarrollador.'};

                default: return { statusCode: 'error', message: InternalError};

            }
        }

        const {statusCode, data, message} = await Models.del({ where });

        if (statusCode !== 'success') return {statusCode, message};

        return { statusCode: 'success', data };

    }catch (error) {

        console.error( { step: "Service del", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function update(params: T.Service.Update.Request): Promise<T.Service.Update.Response> {

    try{

        await Validation.update(params);

        let where: T.Models.Where = { id: params.id };

        const findOne = await Models.findOne({ where });

        if ( findOne.statusCode !== 'success' ){

            switch ( findOne.statusCode ) {

                case 'notFound' : return { statusCode: 'validationError', message: 'No existe el desarrolador.'};

                default: return { statusCode: 'error', message: InternalError};

            }
        }

        const {statusCode, data, message} = await Models.update( params, { where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data[1][0]};

    }catch (error) {

        console.error( { step: "Service update", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function view(params: T.Service.View.Request): Promise<T.Service.View.Response> {

    try{

        await Validation.view(params);

        let options: T.Models.FindAndCountAll.Opts = { where: {} };


        var optionals: T.Models.Attributes[] = ['channel', 'company', 'developer'];

        for( let x of optionals) if (params[x] !== undefined) options.where[x] = params[x];

        for( let x of ['offset', 'limit']) if (params[x] !== undefined) options[x] = params[x];


        const {statusCode, data, message} = await Models.findAndCountAll( options );

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data};

    }catch (error) {

        console.error( { step: "Service view", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function findOne(params: T.Service.FindOne.Request): Promise<T.Service.FindOne.Response> {

    try {

        await Validation.findOne(params);

        let where: T.Models.Where = {};

        var optionals: T.Models.Attributes[] = ['company', 'developer'];

        for( let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findOne(   { where } );

        return { statusCode, data, message};

    }catch (error) {

        console.error( { step: "Service findOne", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}