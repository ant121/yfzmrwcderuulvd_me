const { SyncDB, run } = require('./dist');

async function syncDB() {

    try {
        const result = await SyncDB({force: false});

        if (result.statusCode !== 'success') throw result.message;

        await run();

    } catch (error) { throw error; }

}

async function f() {

    try {

        await syncDB();

    } catch (error) { console.error(error); }

}

f().finally();



