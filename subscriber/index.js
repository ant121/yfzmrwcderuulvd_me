const dotenv = require('dotenv');
const { createClient } = require('redis');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS,
}

const redisClient = createClient({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password
});

async function main() {

    try {

        redisClient.on('error', (err) => console.error('Redis Client Error', err));

        await redisClient.connect();

        await redisClient.pSubscribe('*', async (messagge, channel) => console.log({messagge, channel}))

    }catch (error) { console.error(error);}

}

main();
