import Joi from 'joi';
import * as T from './types';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try{

        const schema = Joi.object({

            developer: Joi.string().required(),
            company: Joi.string().required(),
            individualPerformance: Joi.number().integer().min(0).max(100),
            groupPerformance: Joi.number().integer().min(0).max(100),
            attitudeBehavior: Joi.number().integer().min(0).max(100),
            workgroup: Joi.number().integer().min(0).max(100),
            adaptability: Joi.number().integer().min(0).max(100),
            channel: Joi.string().uuid().required(),

        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validate create", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try{

        const schema = Joi.object({
            developers: Joi.array().items(Joi.string().required()),
            companies: Joi.array().items(Joi.string().required()),
            channels: Joi.array().items(Joi.string().uuid().required()),
        }).or('developers', 'companies','channels');

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validate del", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try{

        const schema = Joi.object({
            id: Joi.number().required(),
            individualPerformance: Joi.number().integer().min(0).max(100),
            groupPerformance: Joi.number().integer().min(0).max(100),
            attitudeBehavior: Joi.number().integer().min(0).max(100),
            workgroup: Joi.number().integer().min(0).max(100),
            adaptability: Joi.number().integer().min(0).max(100),
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validate update", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try{

        const schema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),

            developer: Joi.string(),
            company: Joi.string(),
            channel: Joi.string().uuid(),
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validate view", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const schema = Joi.object({
            developer: Joi.string(),
            company: Joi.string(),
        }).or('developer', 'company');

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validate findOne", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}