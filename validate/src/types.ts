export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';


export namespace Create{

    export interface Request{

        developer: string,
        company: string,
        individualPerformance?: number,
        groupPerformance?: number,
        attitudeBehavior?: number,
        workgroup?: number,
        adaptability?: number,
        channel: string

    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }

}

export namespace Delete{

    export interface Request {

        developers?: string[],
        companies?: string[],
        channels?: string[],

    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }

}

export namespace Update{

    export interface Request {

        id: number;
        individualPerformance?: number,
        groupPerformance?: number,
        attitudeBehavior?: number,
        workgroup?: number,
        adaptability?: number,

    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }

}

export namespace View{

    export interface Request {

        offset?: number;
        limit?: number;

        developer?: string,
        company?: string,
        channel?: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }

}

export namespace FindOne{

    export interface Request {

        developer?: string,
        company?: string,

    }

    export interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }

}
